package no.ntnu.idatt2001.oblig3;

import java.util.ArrayList;

/*
Regn ut summen av alle verdiene av kortene på hånd (ess = 1) 
  
Hent ut bare kort som er av fargen "Hjerter", og vis i et tekstfelt 
på formen "H12 H9 H1". Dersom det ikke er noen Hjerter på hånd, kan
tekstfeltet inneholde teksten "No Hearts", for eksempel.

Sjekk om kortet "Spar dame" finnes blant kortene på hånden.
Sjekk om kortene på hånd utgjør en "5-flush". D.v.s. 5 kort 
av samme farge (5 hjerter eller 5 ruter eller 5 kløver eller 5 spar).
 */

public class CardHand {

    private final ArrayList<PlayCard> hand = new ArrayList<>();

    public CardHand(ArrayList<PlayCard> hand) {
        this.hand.addAll(hand);
    }

    public int getSum() {
        return hand.stream().mapToInt(PlayCard::getFace).sum();
    }

    public String getHearts() {
        return hand.stream()
            .filter(card -> card.getSuit() == 'H')
            .map(PlayCard::getAsString)
            .reduce((s1, s2) -> s1 + " " + s2)
            .orElse("No Hearts");
    }

    public boolean containsQueenOfSpades() {
        return hand.stream().anyMatch(card -> card.getAsString().equals("S12"));
    }

    public boolean isFlush() {
        return hand.stream().map(PlayCard::getSuit).distinct().count() == 1;
    }

    @Override
    public String toString() {
        return hand.stream()
            .map(PlayCard::getAsString)
            .reduce((s1, s2) -> s1 + " " + s2)
            .orElse("");
    }
}
