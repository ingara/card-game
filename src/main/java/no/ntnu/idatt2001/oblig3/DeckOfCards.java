package no.ntnu.idatt2001.oblig3;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {

    private final ArrayList<PlayCard> deck = new ArrayList<>();
    private final char[] suits = { 'S', 'H', 'D', 'C' };

    public DeckOfCards() {
        for (char suit : suits) {
            for (int face = 1; face <= 13; face++) {
                deck.add(new PlayCard(suit, face));
            }
        }
    }

    public ArrayList<PlayCard> dealHand(int n) {
        Random random = new Random();
        ArrayList<PlayCard> hand = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            int index = random.nextInt(deck.size());
            hand.add(deck.remove(index));
        }

        return hand;
    }

    public int getDeckSize() {
        return deck.size();
    }

}
