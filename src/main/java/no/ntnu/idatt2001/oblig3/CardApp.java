package no.ntnu.idatt2001.oblig3;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CardApp extends Application {

    private DeckOfCards deck = new DeckOfCards();
    private CardHand hand;
    
    private TextField handField;
    private TextField sumFaces;
    private TextField cardsOfHearts;
    private TextField flush;
    private TextField queenOfSpades;


    public static void main(String[] args) {
        launch(args);
    }

    private void dealHand() {
        hand = new CardHand(deck.dealHand(5));
        handField.setText(hand.toString());
    }

    private void checkHand() {
        sumFaces.setText(String.valueOf(hand.getSum()));
        cardsOfHearts.setText(hand.getHearts());

        if (hand.isFlush()) {
            flush.setText("Yes");
        } else {
            flush.setText("No");
        }

        if (hand.containsQueenOfSpades()) {
            queenOfSpades.setText("Yes");
        } else {
            queenOfSpades.setText("No");
        }
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Playing Card Game");

        VBox root = new VBox(10);
        root.setPadding(new Insets(10));

        HBox buttons = new HBox(10);
        buttons.alignmentProperty().setValue(Pos.CENTER);
        Button dealButton = new Button("Deal hand");
        Button checkButton = new Button("Check hand");
        buttons.getChildren().addAll(dealButton, checkButton);

        handField = new TextField();
        handField.setEditable(false);
        handField.setAlignment(Pos.CENTER);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.add(new Label("Sum of the faces:"), 0, 0);
        grid.add(new Label("Cards of hearts:"), 2, 0);
        grid.add(new Label("Flush:"), 0, 1);
        grid.add(new Label("Queen of spades:"), 2, 1);

        sumFaces = new TextField();
        sumFaces.setEditable(false);
        cardsOfHearts = new TextField();
        cardsOfHearts.setEditable(false);
        flush = new TextField();
        flush.setEditable(false);
        queenOfSpades = new TextField();
        queenOfSpades.setEditable(false);

        grid.add(sumFaces, 1, 0, 1, 1);
        grid.add(cardsOfHearts, 3, 0, 1, 1);
        grid.add(flush, 1, 1, 1, 1);
        grid.add(queenOfSpades, 3, 1, 1, 1);

        root.getChildren().addAll(buttons, handField, grid);

        dealButton.setOnAction(e -> dealHand());
        checkButton.setOnAction(e -> checkHand());

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
