package no.ntnu.idatt2001.oblig3;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CardHandTest {

    private CardHand cardHand;

    @BeforeEach
    public void setUp() {
        ArrayList<PlayCard> hand = new ArrayList<>();
        hand.add(new PlayCard('S', 1));
        hand.add(new PlayCard('S', 10));
        hand.add(new PlayCard('S', 11));
        hand.add(new PlayCard('S', 12));
        hand.add(new PlayCard('S', 13));
        cardHand = new CardHand(hand);   
    }

    @Test
    public void testGetSum() {
        assertEquals(47, cardHand.getSum());
    }

    @Test
    public void testGetHearts() {
        assertEquals("No Hearts", cardHand.getHearts());
        ArrayList<PlayCard> newHand = new ArrayList<>();
        newHand.add(new PlayCard('H', 1));
        newHand.add(new PlayCard('H', 10));
        newHand.add(new PlayCard('H', 11));
        newHand.add(new PlayCard('H', 12));
        newHand.add(new PlayCard('S', 13));
        cardHand = new CardHand(newHand);
        assertEquals("H1 H10 H11 H12", cardHand.getHearts());
    }

    @Test
    public void testContainsQueenOfSpades() {
        assertEquals(true, cardHand.containsQueenOfSpades());
        ArrayList<PlayCard> newHand = new ArrayList<>();
        newHand.add(new PlayCard('S', 1));
        newHand.add(new PlayCard('S', 10));
        newHand.add(new PlayCard('S', 11));
        newHand.add(new PlayCard('H', 12));
        newHand.add(new PlayCard('S', 13));
        cardHand = new CardHand(newHand);
        assertEquals(false, cardHand.containsQueenOfSpades());
    }
    
}
