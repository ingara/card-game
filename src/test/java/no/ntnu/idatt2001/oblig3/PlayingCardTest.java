package no.ntnu.idatt2001.oblig3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PlayingCardTest {

    @Test
    void testPlayingCard() {
        final char validSuit = 'S';
        final int validFace = 1;
        PlayCard card = new PlayCard(validSuit, validFace);

        assertDoesNotThrow(() -> {
            new PlayCard(validSuit, validFace);
        });

        assertEquals(validSuit, card.getSuit());
        assertEquals(validFace, card.getFace());

        final char invalidSuit = 'X';
        final int invalidFace = 14;
        assertThrows(IllegalArgumentException.class, () -> {
            new PlayCard(invalidSuit, invalidFace);
        });
    }

    @Test
    void testGetAsString() {
        char suit = 'H';
        int face = 4;
        PlayCard card = new PlayCard(suit, face);

        assertEquals("H4", card.getAsString());
    }

    @Test
    void testGetFace() {
        char suit = 'D';
        int face = 13;
        PlayCard card = new PlayCard(suit, face);

        assertEquals(face, card.getFace());
    }

    @Test
    void testGetSuit() {
        char suit = 'C';
        int face = 11;
        PlayCard card = new PlayCard(suit, face);

        assertEquals(suit, card.getSuit());
    }
}
