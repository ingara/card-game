package no.ntnu.idatt2001.oblig3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

public class DeckOfCardsTest {

    @Test
    public void testGetDeckSize() {
        DeckOfCards deck = new DeckOfCards();
        assertEquals(52, deck.getDeckSize());
    }

    @Test
    public void testDealHand() {
        DeckOfCards deck = new DeckOfCards();
        ArrayList<PlayCard> hand = deck.dealHand(5);

        assertEquals(5, hand.size());
        assertEquals(47, deck.getDeckSize());
    }
}
